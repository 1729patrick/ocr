import React, { useEffect, useMemo, useState } from "react";
import ReactDiffViewer from "react-diff-viewer";

import api from "./api";

const App = () => {
  const [checks, setChecks] = useState([]);
  const [outputs, setOutputs] = useState([]);

  const getOutputs = () => {
    api.get("outputs").then(async ({ data }) => {
      const responses = await Promise.all(
        data.data.map(({ path }) =>
          fetch(`http://localhost:3001/static${path}`)
        )
      );

      const texts = await Promise.all(responses.map((t) => t.text()));

      const outs = data.data.map(({ name, path }, index) => ({
        name,
        path,
        text: texts[index],
      }));

      console.log(outs);

      setOutputs(outs);
    });
  };

  useEffect(getOutputs, []);

  const onChange = ({ name, path, text }) => {
    if (!checks.find((check) => check.name === name)) {
      setChecks([...checks, { name, path, text }]);
      return;
    }

    setChecks(checks.filter((check) => check.name !== name));
  };

  const onProcess = () => {
    api.get("process").then(getOutputs);
  };

  const [oldText, newText] = useMemo(() => {
    return checks.map(({ text }) => text);
  }, [checks]);

  return (
    <div>
      <button onClick={onProcess} style={{ marginBottom: 10 }}>
        Process
      </button>
      {outputs.map(({ name, path, text }) => {
        return (
          <div
            key={name}
            style={{
              WebkitUserSelect: "none",
              userSelect: "none",
              display: "flex",
              alignItems: "center",
            }}
          >
            <input
              type="checkbox"
              name={name}
              id={name}
              style={{ cursor: "pointer" }}
              value="Bike"
              onChange={() => onChange({ name, path, text })}
            />
            <label for={name} style={{ marginLeft: 7, cursor: "pointer" }}>
              {name}
            </label>
          </div>
        );
      })}

      {/* <div
        style={{
          marginLeft: -10,
          flexDirection: "row",
          display: "flex",
          overflowX: "scroll",
          width: window.innerWidth,
        }}
      >
        {checks.map(({ path, name }) => {
          return <File path={path} name={name} />;
        })}
      </div> */}

      {oldText && newText && (
        <ReactDiffViewer
          oldValue={oldText}
          newValue={newText}
          splitView={true}
          disableWordDiff
        />
      )}
    </div>
  );
};

export default App;
