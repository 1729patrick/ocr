import React, { useEffect, useState } from "react";

const File = ({ path, name }) => {
  const [text, setText] = useState("");

  useEffect(() => {
    readTextFile();
  }, []);

  const readTextFile = () => {
    console.log(path);
    fetch(`http://localhost:3001/static${path}`)
      .then((r) => r.text())
      .then((text) => {
        setText(text);
      });
  };

  return (
    <div
      style={{
        marginRight: 10,
        marginLeft: 10,
        minWidth: window.innerWidth / 2,
        maxWidth: window.innerWidth / 2,
        overflow: "hidden",
      }}
    >
      <h1
        style={{
          textOverflow: "ellipsis",
          whiteSpace: "nowrap",
          overflow: "hidden",
        }}
      >
        {name}
      </h1>
      <div style={{ border: "1px solid black", padding: 10 }}>
        {text.split("\n").map((item, key) => {
          return (
            <span key={key}>
              {item}
              <br />
            </span>
          );
        })}
      </div>
    </div>
  );
};

export default File;
