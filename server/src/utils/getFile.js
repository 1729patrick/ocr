const md5 = require("md5");

const getFile = (path) => {
  const outputName = md5(path);

  return {
    name: path,
    path: `/${outputName}.txt`,
  };
};

module.exports = getFile;
