const path = require("path");
const saveOutput = require("./saveOutput");

const createIndexes = (outputs) => {
  saveOutput(
    path.join(__dirname, "..", "outputs.json"),
    JSON.stringify({ data: outputs }, null, 4)
  );
};

module.exports = createIndexes;
