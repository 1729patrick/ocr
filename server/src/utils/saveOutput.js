const fs = require("fs");

const saveOutput = (dir, data) => {
  const path = dir
    .split("/")
    .filter((part) => !part.includes(".txt") && !part.includes(".json"))
    .join("/");

  try {
    fs.mkdir(path, { recursive: true }, (err) => {
      if (err) throw err;
    });
  } catch (err) {}

  fs.writeFile(dir, data, function (err) {
    if (err) return console.log(err);
    console.log("OK!");
  });
};

module.exports = saveOutput;
