var pdfUtil = require("pdf-to-text");

const convert = (path) => {
  return new Promise((resolve) => {
    pdfUtil.pdfToText(path, function (err, data) {
      if (err) throw err;
      resolve(data);
    });
  });
};

module.exports = convert;
