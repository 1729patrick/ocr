const fs = require("fs");
const glob = require("glob");

const getFiles = (path) => {
  return new Promise((resolve) => {
    glob(path + "/**/*", (err, res) => {
      if (err) throw err;

      resolve(
        res
          .filter((path) => path.includes(".pdf"))
          .map((p) => p.replace(path, ""))
      );
    });
  });
};

module.exports = getFiles;
