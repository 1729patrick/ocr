const fs = require("fs");
const getFiles = require("./utils/getFiles");
const convert = require("./utils/convert");
const saveOutput = require("./utils/saveOutput");
const path = require("path");

const createIndexes = require("./utils/createIndexes");
const getFile = require("./utils/getFile");

const init = async () => {
  const inputPath = path.join(__dirname, "inputs");

  const outputPath = path.join(__dirname, "outputs");
  fs.rmdirSync(outputPath, { recursive: true });

  const filesPath = await getFiles(inputPath);

  let outputs = [];
  filesPath.forEach(async (filePath, index) => {
    try {
      const output = await convert(`${inputPath}${filePath}`);

      const file = getFile(filePath, outputPath);

      outputs = [...outputs, file];

      saveOutput(`${outputPath}${file.path}`, output);

      if (filesPath.length === index + 1) {
        createIndexes(outputs);
      }
    } catch (e) {
      console.log(e);
    }
  });
};

// init();
module.exports = init;
