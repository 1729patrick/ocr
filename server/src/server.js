const express = require("express");
const path = require("path");
const cors = require("cors");
const outputs = require("./outputs.json");

const main = require("./");

const app = express();
app.use(cors());

app.get("/process", (_, res) => {
  main();
  res.json({ ok: true });
});

app.get("/outputs", (_, res) => {
  res.json(outputs);
});

app.use("/static", express.static(path.join(__dirname, "outputs")));

app.listen(3001);
