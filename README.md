# Como usar?

1. Colocar a pasta com os documentos PDF's em `server/src/inputs`
2. Correr o server com `cd server` e `yarn start`
3. Voltar para a raiz do projeto
4. Correr o front com `cd front` e `yarn start`
5. Clicar no botão "Process", na página web
